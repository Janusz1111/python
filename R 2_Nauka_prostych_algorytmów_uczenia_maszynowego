Rozdział 2: Nauka prostych algorytmów 
Zarys rozdziału
Sztuczne neurony – krótkie spojrzenie na wczesną historię uczenia maszynowego
Formalna definicja sztucznego neuronu
Zasada uczenia się perceptronów
Implementacja algorytmu uczenia się perceptronów w języku Python
Obiektowy interfejs API perceptronu
Trenowanie modelu perceptronu na zbiorze danych Iris
Adaptacyjne neurony liniowe i konwergencja uczenia się
Minimalizacja funkcji kosztowych dzięki opadaniu gradientu
Implementacja adaptacyjnego neuronu liniowego w Pythonie
Poprawianie opadania gradientu dzięki skalowaniu cech
Uczenie maszynowe na dużą skalę i stochastyczne opadanie gradientu
Streszczenie
Uwaga dotycząca korzystania z przykładów kodu
Zalecanym sposobem interakcji z przykładami kodu w tej książce jest skorzystanie z Jupyter Notebook (pliki). Korzystając z Jupyter Notebook, będziesz mógł wykonać kod krok po kroku i mieć wszystkie wynikowe wyniki (w tym wykresy i obrazy) w jednym wygodnym dokumencie..ipynb



Setting up Jupyter Notebook is really easy: if you are using the Anaconda Python distribution, all you need to install jupyter notebook is to execute the following command in your terminal:

conda install jupyter notebook
Then you can launch jupyter notebook by executing

jupyter notebook
A window will open up in your browser, which you can then use to navigate to the target directory that contains the file you wish to open..ipynb

More installation and setup instructions can be found in the README.md file of Chapter 1.

(Even if you decide not to install Jupyter Notebook, note that you can also view the notebook files on GitHub by simply clicking on them: ch02.ipynb)

In addition to the code examples, I added a table of contents to each Jupyter notebook as well as section headers that are consistent with the content of the book. Also, I included the original images and figures in hope that these make it easier to navigate and work with the code interactively as you are reading the book.



When I was creating these notebooks, I was hoping to make your reading (and coding) experience as convenient as possible! However, if you don't wish to use Jupyter Notebooks, I also converted these notebooks to regular Python script files ( files) that can be viewed and edited in any plaintext editor..py
