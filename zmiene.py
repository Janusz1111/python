#! /usr/bin/env python3
# -*- coding: utf-8 -*-
p="PYTHON"
print("""
ZADANIE: Pobierz od użytkownika imię, wiek i powitaj go komunikatem: “Cześć, jestem p mam x lat. Witaj w moim świecie imie. Jesteś starszy(młodszy) ode mnie.”
Poznajemy POJĘCIA: zmienna, wartość, wyrażenie, wejście i wyjście danych, instrukcja warunkowa, komentarz.
deklarujemy i inicjalizujemy zmienne
""")
# deklarujemy i inicjalizujemy zmienne
Rok = 2024
PoczPythonRok = 1989
# obliczamy wiek Pythona
wiekPythona = Rok - PoczPythonRok

# pobieramy dane
imie = input('Jak masz na imię? ')
wiek = int(input('Ile masz lat? '))

# wyświetlamy komunikaty
print("Witaj", imie)
print("Cześź, jestem", p, " mam ", wiekPythona, " lat.")

# instrukcja warunkowa
if wiek > wiekPythona:
    print('Jesteś starszy ode mnie.')
elif wiek == wiekPythona:
    print("Mamy tyle samo lat!")
else:
    print('Jesteś młodszy ode mnie.')
print("""
**********************************************************
*                       >>KONIEC<<                       *  
**********************************************************         
)