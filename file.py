import requests
from bs4 import BeautifulSoup
from urllib.parse import urljoin
import random
from datetime import datetime as dt
import csv
import re
import webbrowser as wb
from urllib import parse as p
import time

def get_higscore(file):
    highscores = ''
    with open(file, 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        header = next(csv_reader)
        highscores_header = "    ########## NAJEPSZE WYNIKI ##########"
        for name, score, date in csv_reader:
            highscores = highscores + '\n' + score + ' ...... ' + name + ' ...... ' + date
    return highscores_header + '\n' + highscores + '\n'

def compare_area(country_area):
    country_area = float(country_area.replace(',', '.'))
    if country_area > 312696:
        area_difference = country_area / 312696
        return f"Polska jest {round(area_difference, 1)} razy mniejsza"
    elif country_area < 312696:
        area_difference = 312696 / country_area
        return f"Polska jest {round(area_difference, 1)} razy większa"
    else:
        return null

def compare_population(country_population):
    country_population = float(country_population)
    if country_population > 41026068:
        population_difference = country_population / 41026068
        return f"W Polsce jest {round(population_difference, 1)} razy mniej ludzi"
    elif country_population < 41026068:
        population_difference = 41026068 / country_population
        return f"W Polsce jest {round(population_difference, 1)} razy więcej ludzi"
    else:
        return null

# Scrapping for flags in decent resolution
flags_url = 'https://pl.wikipedia.org/wiki/Flagi_pa%C5%84stw_%C5%9Bwiata'
flags_response = requests.get(flags_url)
flags_soup = BeautifulSoup(flags_response.content, 'html.parser')
table = flags_soup.find('table', {'class': 'wikitable'})
flags = {}

for row in table.find_all('tr')[1:]:
    columns = row.find_all('td')
    # add a check to make sure the second column contains an image tag
    if columns[1].find('img'):
        country_name = columns[0].text.strip()
        flag_element = columns[1].find('img')
        if flag_element:
            flag_url_relative = flag_element["src"]
            flag_url = urljoin(flags_response.url, flag_url_relative).replace('[/.*px]', '800px')
            flag_url = re.sub(r'[0-9]{1,3}px-Flag', '800px-Flag', flag_url)
    flags[country_name] = {'flag_url': flag_url}

# Scrapping for other information about countries
url = "https://pl.wikipedia.org/wiki/Lista_pa%C5%84stw_%C5%9Bwiata"
response = requests.get(url)
soup = BeautifulSoup(response.content, "html.parser")
rows = soup.select("#mw-content-text div table tbody tr")[1:]

countries = {}

for row in rows:
    columns = row.select("td")
    if len(columns) >= 2:
        name = columns[1].select_one("a").text.strip()
        if len(columns) >= 4:
            continent_element = columns[3].select_one("a")
            capital_element = columns[4].select_one("a")
            area = columns[5].text.strip().replace('\xa0', '') # remove spaces
            population = columns[6].text.strip().replace('\xa0', '')
            if '[' in area:
                area = area.split('[')[0]
            if '[' in population:
                population = population.split('[')[0]
            if capital_element:
                capital = capital_element.text.strip().replace('ʻ', "'").replace('ã', 'a')
            else:
                capital = None
            if continent_element:
                continent = continent_element.text.strip()
            else:
                continent = columns[3].text.strip()
        else:
            capital = None
        countries[name] = {"capital": capital, "continent": continent, "area": area,
                           "population": population}

def get_country_info(country_name):
    # check if country's name includes more than 1 word and concentrate them for the purpose of wiki link
    country_words = country_name.split(' ')
    if len(country_words) > 1:
        country_name_for_wiki_link = '_'.join(country_words)
    else:
        country_name_for_wiki_link = country_name
    country_name_for_wiki_link = p.quote(country_name_for_wiki_link)
    return f"""\n###########################
Ważne informacje o tym kraju to:
Stolica: {countries[country_name]['capital']}
Kontynent: {countries[country_name]['continent']}
Powierzchnia: {countries[country_name]['area']} km2. {compare_area(countries[country_name]['area'])}
Populacja: {countries[country_name]['population']} ludzi. {compare_population(countries[country_name]['population'])}
Więcej informacji znajdziesz tu: https://pl.wikipedia.org/wiki/{country_name_for_wiki_link}
###########################\n\n"""

mode_choice = input("Co chcesz zrobić?\n"
                    "1. Graj w flagi.\n"
                    "2. Graj we flagi na czas\n"
                    "3. Wyświetl info o wybranym kraju.\n")

if mode_choice == '1' or mode_choice == '2':
    score = 0
    running = True

    if mode_choice == '2':
        number_of_flags = int(input("Wpisz liczbę flag: "))
        flags = {k: flags[k] for k in random.sample(list(flags.keys()), number_of_flags)}
        start_time_counter = time.time()

    # repeat the game until the dictionary is empty or the player inputs 'koniec'
    while flags and running:
        # get a random country from the dictionary
        country = random.choice(list(flags.keys()))
        wb.open(flags[country]["flag_url"], autoraise=False)

        hint_request = True
        choice_given = False
        hint_counter = 1
        multiply_choice_list = []

        # show the flag and ask for the country's name
        while hint_request:
            player_answer = input("Wpisz poprawną nazwę kraju na podstawie jego flagi: ").lower()

            # Ask for hint
            if player_answer == "literka":
                print(f"Nazwa tego kraju zaczyna się na: {country[0:hint_counter]}")
                hint_counter += 1
                continue

            # Ask for choice
            elif player_answer == 'wybór' and choice_given == True:
                print("Ten rodzaj pomocy można wybrać tylko raz!")
            elif player_answer == 'wybór' and choice_given == False:
                multiply_choice_list.append(country)
                while len(multiply_choice_list) < 3:
                    random_country = random.choice(list(flags.keys()))
                    if random_country not in multiply_choice_list:
                        multiply_choice_list.append(random_country)
                random.shuffle(multiply_choice_list)

                for item in enumerate(multiply_choice_list, 1):
                    print(item)
                choice_given = True
                continue
            elif player_answer == 'punkty':
                print("Twój aktualny wynik to: ", score)
            elif player_answer == 'tabela':
                print(get_higscore('highscores.csv'))
            else:
                hint_request = False


        # if the guess is correct, print information about the country and remove the guessed country from the
        # dictionary

        if player_answer == country.lower():
            print("Brawo! Zdobywasz punkt!")
            print(get_country_info(country))
            score += 1
            del flags[country]
        elif player_answer == 'koniec':
            running = False
        else:
            print(f"Niestety nie :(. Poprawna odpowiedź to: '{country}'.")

    # Ask the player for their name
    players_name = input("Please enter your name: ")

    # Print players elapsed time
    if mode_choice == '2':
        end_time_counter = time.time()
        print(f"Odgadłeś wszystkie flagi w czasie {round(end_time_counter - start_time_counter, 2)} sek")

    # Print the player's score
    elif mode_choice == '1':
        print(f"{players_name}, twój końcowy wynik to: {score}")


    # Save the player's name and score to a file
    with open("highscores.csv", "a") as f:
        f.write(f"\n{players_name};{score};{dt.now().strftime('%d-%m-%Y %H:%M:%S')}")

    # Sort Highscores
    new_file_content = 'name;score;date'
    with open('highscores.csv', 'r') as csv_file_readonly:
        csv_reader = csv.reader(csv_file_readonly, delimiter=';')
        header = next(csv_reader)
        sorted_rows = sorted(csv_reader, key=lambda row: int(row[1]), reverse=True)
        with open('highscores.csv', 'w') as csv_file_writable:
            for row in sorted_rows:
                highscore_entry = f'{row[0]};{row[1]};{row[2]}'
                new_file_content = new_file_content + '\n' + highscore_entry
            csv_file_writable.write(new_file_content)

    # Print Highscores
    print(get_higscore('highscores.csv'))

elif mode_choice == '3':
    running = True
    while running:
        country = input("Wpisz nazwę kraju: ").title()
        if country == 'Koniec':
            running = False
        else:
            print(get_country_info(country))
