# Python

Python to dynamicznie typowanyy język interpretowany wysokiego poziomu. Cechuje się czytelnością i zwięzłością kodu. Stworzony został w latach 90. przez Guido van Rossuma, nazwa zaś pochodzi od tytułu serialu komediowego emitowanego w BBC pt. “Latający cyrk Monty Pythona”.
Python może być dowolnie rozszerzany dzięki licznym bibliotekom, które pozwalają tworzyć aplikacje matematyczne (Matplotlib), okienkowe (np. PyQt, PyGTK, wxPython), internetowe (Flask, Django) czy multimedialne i gry (Pygame).

## Aplikacje WWW (Django)
Python znakomicie nadaje się do tworzenia aplikacji internetowych dzięki frameworkowi **Django**. Upraszcza on projektowanie serwisów oferując gotowe rozwiązania wielu powtarzalnych i pracochłonnych mechanizmów wymaganych w serwisach internetowych. Oferuje również gotowe środowisko testowe, czyli deweloperski serwer WWW, nie musimy instalować żadnych dodatkowych narzędzi typu LAMP (WAMP).
## Gry w Pythonie
**Pygame** to zbiór modułów w języku Python wpomagających tworzenie aplikacji multimedialnych, zwłaszcza gier. Wykorzystuje możliwości biblioteki SDL (Simple DirectMedia Layer), jest darmowy i rozpowszechniany na licencji GNU General Public Licence. Działa na wielu platformach i systemach operacyjnych.
## Aplikacje WWW (Flask)
Python znakomicie nadaje się do tworzenia aplikacji internetowych dzięki takim rozszerzeniom jak micro-framework** Flask**. Upraszcza on projektowanie zapewniając przejrzysty schemat łączenia adresów URL, żródła danych, widoków i szablonów. Domyślnie dostajemy również deweloperski serwer WWW, nie musimy instalować żadnych dodatkowych narzędzi typu LAMP (WAMP).
## Słownik
**aplikacja** - program komputerowy.
**framework** - zestaw komponentów i bibliotek wykorzystywany do budowy aplikacji, przykładem jest biblioteka Pythona Flask.
**HTML** -język znaczników wykorzystywany do formatowania dokumentów, zwłaszcza stron WWW.
**CSS** - język służący do opisu formy prezentacji stron WWW.
**HTTP** - protokół przesyłania dokumentów WWW.
**GET** - typ żądania HTTP, służący do pobierania zasobów z serwera WWW.
**POST** - typ żądania HTTP, służący do umieszczania zasobów na serwerze WWW.
**Kod odpowiedzi HTTP** -numeryczne oznaczenie stanu realizacji zapytania klienta, np. 200 (OK) lub 404 (Not Found). 
**logowanie** - proces autoryzacji i uwierzytelniania użytkownika w systemie.
**ORM** - (ang. Object-Relational Mapping) – mapowanie obiektowo-relacyjne, oprogramowanie odwzorowujące strukturę relacyjnej bazy danych na obiekty danego języka oprogramowania.
**Peewee** - prosty i mały system ORM, wspiera Pythona w wersji 2 i 3, obsługuje bazy SQLite3, MySQL, Posgresql.
SQLAlchemy
rozbudowany zestaw narzędzi i system ORM umożliwiający wykorzystanie wszystkich możliwości SQL-a, obsługuje bazy SQLite3, MySQL, Postgresql, Oracle, MS SQL Server i inne.
**serwer deweloperski** - testowy serwer www używany w czasie prac nad oprogramowaniem.
**serwer WWW** - serwer obsługujący protokół HTTP.
**baza danych** - program przeznaczony do przechowywania i przetwarzania danych.
**szablon** - wzorzec (nazywany czasem templatką) strony WWW wykorzystywany do renderowania widoków.
**renderowanie szablonu** - przetwarzanie szkieletowego kodu HTML oraz specjalnych tagów w celu uzyskania kompletnego kodu HTML strony zawierającego przekazane do szablonu dane.
**URL** - ustandaryzowany format adresowania zasobów w internecie (przykład).
**MVC** - (ang. Model-View-Controller) – Model-Widok-Kontroler, wzorzec projektowania aplikacji rozdzielający dane (model) od sposobu ich prezentacji (widok) i zarządzania ich przepływem (kontroler).
**model** - schemat opisujący strukturę danych w bazie, np. klasa definiująca tabele i relacje między nimi. W
**widok** - we Flasku lub Django jest to funkcja lub klasa, która obsługuje żądania wysyłane przez użytkownika, przeprowadza operacje na danych i najczęściej zwraca je np. w formie strony WWW do przeglądarki.
**kontroler** - logika aplikacji, we Flasku lub Django mechanizm obsługujący zadania HTTP powiązane z określonymi adresami URL za pomocą widoków (funkcji lub klas).
**sesja** - w kontekście aplikacji wykorzystujących protokół HTTP sposób zapamiętywania po stronie serwera danych związanych z konkretnym użytkownikiem.
**ciasteczka** - (ang. cookies) zaszyfrowane dane tekstowe wysyłane przez serwer i zapamiętywane po stronie klienta, zawierają np. identyfikator sesji użytkownika.

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Janusz1111/python.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://docs.gitlab.com/ee/user/application_security/sast/)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:d29bf09a8d4d9403ee10e961e34000bc?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

