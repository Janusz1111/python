from mysql.connector import (
    MySQLConnection as db,
    errorcode,
    Error
)


class MySQLDataBase:
    def connection(self, dbConfig):
        try:
            print('Łączenie z bazą danych MySQL...')
            conn = db(**dbConfig)
            print("Połączenie nawiązane.")
            return conn
        except Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Coś jest nie tak z Twoją nazwą użytkownika lub hasłem!")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Baza danych nie istnieje!")
            else:
                print(err)
        else:
            conn.close()
            print("Połączenie zamknięte.")
